package com.atlassian.bitbucket.coverage.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.coverage.CoverageParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AoCodeCoverageDaoTest {
    private static final String ANY_COMMIT_ID = "";
    private static final String ANY_PATH = "";
    private static final String EMPTY_COVERAGE = "";

    @Mock
    private ActiveObjects ao;
    @Spy
    private CoverageParser coverageParser;
    @InjectMocks
    private AoCodeCoverageDao aoCodeCoverageDao;

    @Test
    public void findFileCoverageReturnsNullWhenNoCoverageFound() {
        when(ao.find(any(Class.class), any(String.class), any(String.class))).thenReturn(new AoFileCoverage[0]);

        AoFileCoverage result = aoCodeCoverageDao.findFileCoverage(ANY_COMMIT_ID, ANY_PATH);

        assertThat(result, nullValue());
    }

    @Test
    public void createsNewCoverageWhenNoCoverageFound() {
        when(ao.find(any(Class.class), any(String.class), any(String.class))).thenReturn(new AoFileCoverage[0]);

        aoCodeCoverageDao.addFileCoverage(ANY_COMMIT_ID, ANY_PATH, EMPTY_COVERAGE);

        verify(ao).create(any(Class.class), anyMap());
    }

}
