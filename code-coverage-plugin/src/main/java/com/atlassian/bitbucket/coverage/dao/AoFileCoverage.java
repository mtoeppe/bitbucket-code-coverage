package com.atlassian.bitbucket.coverage.dao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

import static net.java.ao.schema.StringLength.UNLIMITED;

public interface AoFileCoverage extends Entity {

    String COMMIT_ID_COLUMN = "COMMIT_ID";
    String PATH_COLUMN = "PATH";
    String COVERED_COLUMN = "COVERED";
    String PARTLY_COVERED_COLUMN = "PARTLY_COVERED";
    String UNCOVERED_COLUMN = "UNCOVERED";

    @Accessor(PATH_COLUMN)
    String getPath();

    @Indexed
    @Accessor(COMMIT_ID_COLUMN)
    @NotNull
    String getCommitId();

    @Accessor(COVERED_COLUMN)
    @StringLength(UNLIMITED)
    String getCovered();

    @Mutator(COVERED_COLUMN)
    void setCovered(String coverage);

    @Accessor(PARTLY_COVERED_COLUMN)
    @StringLength(UNLIMITED)
    String getPartlyCovered();

    @Mutator(PARTLY_COVERED_COLUMN)
    void setPartlyCovered(String coverage);

    @Accessor(UNCOVERED_COLUMN)
    @StringLength(UNLIMITED)
    String getUnCovered();

    @Mutator(UNCOVERED_COLUMN)
    void setUnCovered(String coverage);

}
