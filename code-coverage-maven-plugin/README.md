Code Coverage Maven Plugin
==========================

Maven plugin to convert and publish coverage data to BitBucket server.

Usage
=====

```bash
mvn com.atlassian.bitbucket:code-coverage-maven-plugin:publish
    -Dbitbucket.url=https://your-bitbucket
    -Dbitbucket.user=user
    -Dbitbucket.password=password
    -Dbitbucket.commit.id=commidId
    -Dcoverage.file=path/to/coverage/file
```

There are also optional parameters:

* `coverage.format` - supported formats are JACOCO (xml), COBERTURA (xml) and LCOV. Default value is LCOV.
* `project.directory` - some tools put absolute file name in coverage report, so we nee to convert them to relative
in order to be recognized inside remote repo. Default value is current directory, so you don't need to set it in most
cases.
* `bitbucket.timeout` - timeout in millis for interaction with Bitbucket. Processing coverage information for big 
project could require some time, so set this property if you get `java.net.SocketTimeoutException` error. Default value is 15000 (15s).

To access Bitbucker REST API, you need to provide authentication data. There are two options to do this:

* with username and password by setting `bitbucket.user` and `bitbucket.password` properties
* with [personal access token](https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html) by setting `bitbucket.token` property
