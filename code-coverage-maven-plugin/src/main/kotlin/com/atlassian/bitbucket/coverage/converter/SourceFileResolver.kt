package com.atlassian.bitbucket.coverage.converter

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.FileSystems
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitResult.CONTINUE
import java.nio.file.FileVisitResult.TERMINATE
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.PathMatcher
import java.nio.file.Paths
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes


class SourceFileResolver(private val projectPath: Path) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(SourceFileResolver.javaClass)
    }

    fun resolveSource(packageName: String, fileName: String): String {
        val tail  = Paths.get(packageName).resolve(fileName)
        val fs = FileSystems.getDefault()
        val matcher = fs.getPathMatcher("glob:**/$tail")
        return resolvePattern(matcher, tail.toString())
    }

    fun resolveClass(className: String): String {
        logger.debug("Looking up for file with class {}", className)
        val unNestedClassName = className.split("$")[0]

        val fs = FileSystems.getDefault()
        val matcher = fs.getPathMatcher("glob:**/$unNestedClassName.*")
        return resolvePattern(matcher, className)
    }

    fun resolveFileName(fileName: String): String {
        logger.debug("Looking up for file {}", fileName)

        val fs = FileSystems.getDefault()
        val matcher = fs.getPathMatcher("glob:**/$fileName")
        return resolvePattern(matcher, fileName)
    }

    fun resolvePattern(matcher: PathMatcher, default: String): String {
        val fileVisitor = TailFileVisitor(matcher)

        Files.walkFileTree(projectPath, fileVisitor)
        return if (fileVisitor.fileFound != null) {
            val result = projectPath.relativize(fileVisitor.fileFound).normalize()
            logger.debug("File found at {}", result)
            result.toString()
        } else {
            logger.debug("File not found, falling back to {}", default)
            default
        }
    }

    class TailFileVisitor(private val matcher: PathMatcher): SimpleFileVisitor<Path>() {
        var fileFound: Path? = null

        override fun visitFile(file: Path, attributes: BasicFileAttributes): FileVisitResult {
            return if (matcher.matches(file)) {
                fileFound = file
                TERMINATE
            } else {
                CONTINUE
            }
        }
    }

}
