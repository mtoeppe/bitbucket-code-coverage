package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity
import java.io.File
import java.nio.file.Path

abstract class AbstractCoverageConverter {

    companion object {
        const val COVERED_KEY = "covered"
        const val PARTLY_COVERED_KEY = "partly"
        const val UNCOVERED_KEY = "uncovered"
    }

    abstract fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity

    fun convertFileCoverage(filePath: String, fileCoverageMap: Map<String, Set<String>>): FileCoverageEntity? {
        val coveredString = joinLines(fileCoverageMap[COVERED_KEY])
        val partlyCoveredString = joinLines(fileCoverageMap[PARTLY_COVERED_KEY])
        val uncoveredString = joinLines(fileCoverageMap[UNCOVERED_KEY])

        if (emptyCoverage(coveredString, partlyCoveredString, uncoveredString)) {
            return null
        }

        val result = FileCoverageEntity()
        result.path = filePath
        result.coverage = "C:$coveredString;P:$partlyCoveredString;U:$uncoveredString"

        return result
    }

    fun emptyCoverage(covered: String, partly: String, uncovered: String): Boolean {
        return covered.isEmpty() && partly.isEmpty() && uncovered.isEmpty()
    }

    fun joinLines(coverageList: Set<String>?): String {
        return coverageList?.joinToString(separator = ",").orEmpty()
    }

    fun emptyCoverageMap(): HashMap<String, MutableSet<String>> {
        return hashMapOf(
                COVERED_KEY to mutableSetOf(),
                PARTLY_COVERED_KEY to mutableSetOf(),
                UNCOVERED_KEY to mutableSetOf()
        )
    }
}
