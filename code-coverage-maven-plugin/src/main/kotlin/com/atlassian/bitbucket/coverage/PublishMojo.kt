package com.atlassian.bitbucket.coverage

import com.atlassian.bitbucket.coverage.CoverageFormat.valueOf
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter
import com.atlassian.bitbucket.coverage.rest.Client
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.github.kittinunf.result.failure
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import java.io.File
import java.lang.IllegalArgumentException
import java.net.URL
import java.nio.file.Path
import java.nio.file.Paths

@Mojo(name = "publish", requiresProject = false)
class PublishMojo : AbstractMojo() {
    companion object {
        private const val POST_FAILED = "Failed to post coverage info to bitbucket: %s"
        private const val UNKNOWN_FORMAT = "Unknown coverage format: %s"
    }

    @Parameter(property = "bitbucket.url", required = true)
    private val bitbucketUrl: URL? = null

    @Parameter(property = "bitbucket.user", required = false)
    private val user: String = ""

    @Parameter(property = "bitbucket.password", required = false)
    private val password: String = ""

    @Parameter(property = "bitbucket.token", required = false)
    private val token: String = ""

    @Parameter(property = "bitbucket.commit.id", required = true)
    private val commitId: String = ""

    @Parameter(property = "bitbucket.timeout", required = false)
    private val timeout: Int? = null

    @Parameter(property = "coverage.file", required = true)
    private val coverageFile: String = ""

    @Parameter(property = "coverage.format")
    private val _coverageFormat: String = "LCOV"

    @Parameter(property = "project.directory")
    private var projectDirectory: String = "."

    override fun execute() {
        val coverageFormat: CoverageFormat
        try {
            coverageFormat = valueOf(_coverageFormat)
        } catch (e: IllegalArgumentException) {
            throw MojoFailureException(UNKNOWN_FORMAT.format(_coverageFormat))
        }

        val projectPath: Path = Paths.get(projectDirectory).toAbsolutePath()
        val coverageConverter: AbstractCoverageConverter = coverageFormat.converter
        val coverageEntity: CommitCoverageEntity = coverageConverter.convert(File(coverageFile), projectPath)

        log.debug("Coverage converted")

        val restClient = Client(bitbucketUrl!!, user, password, token, timeout)
        val result = restClient.postCoverage(commitId, coverageEntity)

        result.failure {
            throw MojoFailureException(POST_FAILED.format(result!!))
        }

        log.debug("Bitbucket response: ${result.get()}")
    }

}
