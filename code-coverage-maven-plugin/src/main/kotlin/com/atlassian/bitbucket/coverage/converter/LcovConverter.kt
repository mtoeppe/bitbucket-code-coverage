package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

class LcovConverter: AbstractCoverageConverter() {
    companion object {
        const val EOR = "end_of_record"
        const val SF = "SF"
        const val DA = "DA"
        const val BRDA = "BRDA"
        const val BRF = "BRF"
        const val BRH = "BRH"
        const val FN = "FN"
        const val FNDA = "FNDA"

        val logger: Logger = LoggerFactory.getLogger(LcovConverter.javaClass)
    }

    override fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity {
        val files = mutableListOf<FileCoverageEntity?>()

        val fileLines = mutableListOf<String>()
        for (line: String in coverageFile.readLines()) {
            val trimmedLine = line.trim()

            logger.debug(trimmedLine)
            if (trimmedLine == EOR) {
                val fileCoverageEntity = convertFileLines(fileLines, projectPath)
                files.add(fileCoverageEntity)
                fileLines.clear()
            } else {
                fileLines.add(trimmedLine)
            }

        }

        val result = CommitCoverageEntity()
        result.files = files.filterNotNull()
        return result
    }

    fun convertFileLines(fileLines: List<String>, projectPath: Path): FileCoverageEntity? {
        var filePath = Paths.get("")

        val fileCoverageMap = emptyCoverageMap()
        val functions = hashMapOf<String, String>()

        for (line: String in fileLines) {
            val (type, data) = line.split(":")

            when (type) {
                SF -> {
                    filePath = resolveAndRelativize(data, projectPath)
                }

                DA -> {
                    val (key, lineNum) = processDAEntry(data)
                    fileCoverageMap[key]!!.add(lineNum)
                }

                BRDA -> {
                    val (key, lineNum) = processBRDAEntry(data, fileCoverageMap)
                    fileCoverageMap[key]!!.add(lineNum)
                }

                FN -> {
                    val (lineNum, functionName) = data.split(",")
                    functions[functionName] = lineNum
                }

                FNDA -> {
                    val (functionHits, functionName) = data.split(",")
                    val key = keyFromHits(functionHits)
                    val lineNum = functions[functionName]
                    fileCoverageMap[key]!!.add(lineNum!!)
                }

                BRF -> {
                    // we don't care about file branches total
                }

                BRH -> {
                    // we don't care about file branches covered
                }

            }
        }

        return convertFileCoverage(filePath.toString(), fileCoverageMap)
    }

    fun isTaken(taken: String): Boolean {
        val hits = taken.toIntOrNull() ?: return false
        return hits > 0
    }

    fun resolveAndRelativize(rawPath: String, projectPath: Path): Path {
        var result = Paths.get(rawPath)
        if (result.isAbsolute) {
            result = projectPath.relativize(result)
        }
        return result
    }

    fun processDAEntry(data: String): Pair<String, String> {
        val (lineNum, lineHits) = data.split(",")
        val key = keyFromHits(lineHits)
        return Pair(key, lineNum)
    }

    fun processBRDAEntry(data: String, fileCoverageMap: Map<String, MutableSet<String>>):
            Pair<String, String> {

        val (lineNum, _, _, hits) = data.split(",")
        var key = PARTLY_COVERED_KEY
        if (!fileCoverageMap[PARTLY_COVERED_KEY]!!.contains(lineNum)) {
            if (fileCoverageMap[COVERED_KEY]!!.contains(lineNum)) {
                if (isTaken(hits)) {
                    key = COVERED_KEY
                } else {
                    fileCoverageMap[COVERED_KEY]!!.remove(lineNum)
                }
            } else if (fileCoverageMap[UNCOVERED_KEY]!!.contains(lineNum)) {
                if (isTaken(hits)) {
                    fileCoverageMap[UNCOVERED_KEY]!!.remove(lineNum)
                } else {
                    key = UNCOVERED_KEY
                }
            } else {
                key = keyFromHits(hits)
            }
        }

        return Pair(key, lineNum)
    }

    private fun keyFromHits(hits: String): String {
        return if (isTaken(hits)) COVERED_KEY else UNCOVERED_KEY
    }

}
