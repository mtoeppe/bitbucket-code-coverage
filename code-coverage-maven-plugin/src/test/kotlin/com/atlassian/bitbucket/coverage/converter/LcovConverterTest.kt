package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.PARTLY_COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.UNCOVERED_KEY
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.empty
import org.junit.Assert.assertFalse
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Test
import java.nio.file.Paths

class LcovConverterTest {
    private val lcovConverter = LcovConverter()
    private val fileCoverageMap = HashMap<String, MutableSet<String>>()

    @Test
    fun isTakenReturnsFalseForEmptyString() {
        assertFalse(lcovConverter.isTaken(""))
    }

    @Test
    fun isTakenReturnsFalseForDash() {
        assertFalse(lcovConverter.isTaken("-"))
    }

    @Test
    fun isTakenReturnsFalseForZero() {
        assertFalse(lcovConverter.isTaken("0"))
    }

    @Test
    fun isTakenReturnsTrueForPositiveNumber() {
        assertTrue(lcovConverter.isTaken("2"))
    }

    @Test
    fun resolveAndRelativizeRelativePath() {
        val result = lcovConverter.resolveAndRelativize("a/b/c", Paths.get("/a/b"))
        assertThat(result.toString(), `is`("a/b/c"))
    }

    @Test
    fun resolveAndRelativizeAbsolutePath() {
        val result = lcovConverter.resolveAndRelativize("/a/b/c", Paths.get("/a/b"))
        assertThat(result.toString(), `is`("c"))
    }

    @Test
    fun processDAEntry() {
        val (key, lineNum) = lcovConverter.processDAEntry("1,1")
        assertThat(key, `is`(COVERED_KEY))
        assertThat(lineNum, `is`("1"))
    }

    @Test
    fun processBRDAEntryFirstCoveredEntry() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf()
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf()
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,4", fileCoverageMap)
        assertThat(key, `is`(COVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[UNCOVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[PARTLY_COVERED_KEY]!!, empty())
    }

    @Test
    fun processBRDAEntryFirstUncoveredEntry() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf()
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf()
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,-", fileCoverageMap)
        assertThat(key, `is`(UNCOVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[COVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[PARTLY_COVERED_KEY]!!, empty())
    }

    @Test
    fun processBRDAEntryTwoCoveredEntries() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf("1")
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf()
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,4", fileCoverageMap)
        assertThat(key, `is`(COVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[UNCOVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[PARTLY_COVERED_KEY]!!, empty())
    }

    @Test
    fun processBRDAEntryTwoUncoveredEntries() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf()
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf("1")
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,-", fileCoverageMap)
        assertThat(key, `is`(UNCOVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[COVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[PARTLY_COVERED_KEY]!!, empty())
    }

    @Test
    fun processBRDAUncoveredAfterCoveredReturnsPartlyCoveredAndRemovesCoveredEntry() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf("1")
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf()
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,-", fileCoverageMap)
        assertThat(key, `is`(PARTLY_COVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[COVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[UNCOVERED_KEY]!!, empty())
    }

    @Test
    fun processBRDACoveredAfterUncoveredReturnsPartlyCoveredAndRemovesUncoveredEntry() {
        fileCoverageMap[COVERED_KEY] = mutableSetOf()
        fileCoverageMap[UNCOVERED_KEY] = mutableSetOf("1")
        fileCoverageMap[PARTLY_COVERED_KEY] = mutableSetOf()
        val (key, lineNumber) = lcovConverter.processBRDAEntry("1,2,3,4", fileCoverageMap)
        assertThat(key, `is`(PARTLY_COVERED_KEY))
        assertThat(lineNumber, `is`("1"))
        assertThat(fileCoverageMap[COVERED_KEY]!!, empty())
        assertThat(fileCoverageMap[UNCOVERED_KEY]!!, empty())
    }

}
