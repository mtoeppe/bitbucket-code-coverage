package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.PARTLY_COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.UNCOVERED_KEY
import com.atlassian.bitbucket.coverage.jacoco.Counter
import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.io.File
import java.nio.file.Paths

class JacocoConverterTest {
    private val ANY_STRING = "anystring"

    private val jacocoConverter = JacocoConverter()

    @Test
    fun readJacocoXmlReport() {
        val reportFile = File(this::class.java.classLoader.getResource("jacoco-report.xml").file)
        jacocoConverter.convert(reportFile, Paths.get(""))
    }

    @Test
    fun getBlockCoverageStateCovered() {
        assertThat(jacocoConverter.getBlockCoverageState("0", "1"), `is`(COVERED_KEY))
    }

    @Test
    fun getBlockCoverageStateUncovered() {
        assertThat(jacocoConverter.getBlockCoverageState("1", "0"), `is`(UNCOVERED_KEY))
    }

    @Test
    fun getBlockCoverageStatePartly() {
        assertThat(jacocoConverter.getBlockCoverageState("1", "1"), `is`(PARTLY_COVERED_KEY))
    }

    @Test
    fun getLineCoverageStateCovered() {
        assertThat(jacocoConverter.getLineCoverageState("0"), `is`(COVERED_KEY))
    }

    @Test
    fun getLineCoverageStateUncovered() {
        assertThat(jacocoConverter.getLineCoverageState("1"), `is`(UNCOVERED_KEY))
    }

    @Test
    fun isSimpleStatementSimle() {
        assertThat(jacocoConverter.isSimpleStatement("0", "0"), `is`(true))
    }

    @Test
    fun isSimpleStatementNotSimpleWithMissedBranches() {
        assertThat(jacocoConverter.isSimpleStatement("1", "0"), `is`(false))
    }

    @Test
    fun isSimpleStatementNotSimpleWithCoveredBranches() {
        assertThat(jacocoConverter.isSimpleStatement("0", "1"), `is`(false))
    }

    @Test(expected = IllegalArgumentException::class)
    fun getCoverageThrowsExceptionForInvalidCounters() {
        val counters = listOf<Counter>()
        jacocoConverter.getCoverage(ANY_STRING, counters)
    }

    @Test
    fun getCoverage() {
        val coveredExpected = "1"
        val missedExpected = "2"

        val testCounter: Counter = Mockito.mock(Counter::class.java)
        `when`(testCounter.type).thenReturn(ANY_STRING)
        `when`(testCounter.covered).thenReturn(coveredExpected)
        `when`(testCounter.missed).thenReturn(missedExpected)

        val counters = mutableListOf<Counter>()
        counters.add(testCounter)
        val (covered, missed) = jacocoConverter.getCoverage(ANY_STRING, counters)

        assertThat(covered, `is`(coveredExpected))
        assertThat(missed, `is`(missedExpected))
    }

}
