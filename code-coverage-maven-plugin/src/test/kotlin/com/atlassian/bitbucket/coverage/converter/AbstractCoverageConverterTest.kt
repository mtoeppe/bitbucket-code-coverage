package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test
import java.io.File
import java.nio.file.Path

class AbstractCoverageConverterTest {

    class Converter: AbstractCoverageConverter() {
        override fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    private val converter: Converter = Converter()

    @Test
    fun convertFileCoverageReturnsNullForEmptyCoverage() {
        val result = converter.convertFileCoverage("", converter.emptyCoverageMap())
        assertThat(result, nullValue())
    }

    @Test
    fun joinLinesReturnsEmptyStringForEmptySet() {
        assertThat(converter.joinLines(setOf()), CoreMatchers.`is`(""))
    }

    @Test
    fun joinLines() {
        assertThat(converter.joinLines(setOf("1", "2")), CoreMatchers.`is`("1,2"))
    }

}
